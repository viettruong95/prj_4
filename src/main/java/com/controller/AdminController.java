package com.controller;

import com.common.constProject;
import com.config.FirestoreConfig;
import com.entity.Event;
import com.entity.Order;
import com.entity.Session;
import com.entity.User;
import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.firebase.cloud.FirestoreClient;
import com.util.RESTUtil;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

@Controller
public class AdminController {
    private final static String registerSuccess = "Successful registration";
    private final static String checkAccount = "Account not admin";
    private final static String checkAccExists = "Account already exists";
    private final static int roleAdmin = 2;

    @RequestMapping(value = "create", method = RequestMethod.GET)
    String getRegister(Model model) throws ExecutionException, InterruptedException {
        return "addev";
    }

    @RequestMapping(value = "admin", method = RequestMethod.GET)
    String getView(Model model) throws ExecutionException, InterruptedException {
        return "admin";
    }

    @RequestMapping(value = "create", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    String postRegister(Model model, @RequestBody() String content) throws ExecutionException, InterruptedException {
        System.out.println(content);
        Firestore db = FirestoreClient.getFirestore(FirestoreConfig.app);
        JSONObject object = new JSONObject(content);
        Map<String, Object> map = new HashMap<>();
                Event event = new Event();
                event.setTitle(object.getString("title"));
                event.setDescription(object.getString("description"));
                event.setAddress(object.getString("address"));
                event.setPlace(object.getString("place"));
                event.setPrice(object.getLong("price"));
                event.setQuantity(object.getLong("quantity"));
                event.setStart_at(RESTUtil.parLongTime(object.getString("start_at")));
                event.setTime(object.getLong("time"));
                event.setType(object.getString("type_eve"));
                event.setImage(object.getString("image"));
                db.collection(constProject.eventStr).document(event.getId()).set(event);
                map.put(constProject.statusStr, 1);
                map.put(constProject.msgStr, registerSuccess);
        return RESTUtil.parJson(map);
    }


    @RequestMapping(value = "listorder{page}{session}", method = RequestMethod.GET, produces = "application/json")
    String getOrder(Model model, @RequestParam("page") String page, @RequestParam("session") String session) throws ExecutionException, InterruptedException {

        Firestore db = FirestoreClient.getFirestore(FirestoreConfig.app);

        ApiFuture<QuerySnapshot> futureSession = db.collection("session").whereEqualTo("jsession", session).get();
        List<Session> sessions = futureSession.get().toObjects(Session.class);

        if (sessions.size() > 0 && ((sessions.get(0).getCreateTime() + 30 * 60 * 1000) > System.currentTimeMillis())) {
            ApiFuture<DocumentSnapshot> futureUser = db.collection("user").document(sessions.get(0).getPhone()).get();
            User user = futureUser.get().toObject(User.class);
            if (user != null && user.getStatus() == roleAdmin) {
                ApiFuture<QuerySnapshot> futureOrder = db.collection("order").limit(20).get();
                List<Order> orders = futureOrder.get().toObjects(Order.class);
                model.addAttribute("orders", orders);
                return "listorder";
            } else {
                return "index";
            }
        } else {
            return "login";
        }

    }


    @RequestMapping(value = "listuser{page}{session}", method = RequestMethod.GET, produces = "application/json")
    String getUser(Model model, @RequestParam("page") String page, @RequestParam("session") String session) throws ExecutionException, InterruptedException {

        Firestore db = FirestoreClient.getFirestore(FirestoreConfig.app);

        ApiFuture<QuerySnapshot> futureSession = db.collection("session").whereEqualTo("jsession", session).get();
        List<Session> sessions = futureSession.get().toObjects(Session.class);

        if (sessions.size() > 0 && ((sessions.get(0).getCreateTime() + 30 * 60 * 1000) > System.currentTimeMillis())) {
            ApiFuture<DocumentSnapshot> futureUser = db.collection("user").document(sessions.get(0).getPhone()).get();
            User user = futureUser.get().toObject(User.class);
            if (user != null && user.getStatus() == roleAdmin) {
                ApiFuture<QuerySnapshot> futureUser1 = db.collection("user").limit(20).get();
                List<User> users = futureUser1.get().toObjects(User.class);
                model.addAttribute("users", users);
                return "listuser";
            } else {
                return "index";
            }
        } else {
            return "login";
        }

    }
    @RequestMapping(value = "listevent{page}{session}", method = RequestMethod.GET, produces = "application/json")
    String getEvent(Model model, @RequestParam("page") String page, @RequestParam("session") String session) throws ExecutionException, InterruptedException {

        Firestore db = FirestoreClient.getFirestore(FirestoreConfig.app);

        ApiFuture<QuerySnapshot> futureSession = db.collection("session").whereEqualTo("jsession", session).get();
        List<Session> sessions = futureSession.get().toObjects(Session.class);

        if (sessions.size() > 0 && ((sessions.get(0).getCreateTime() + 30 * 60 * 1000) > System.currentTimeMillis())) {
            ApiFuture<DocumentSnapshot> futureUser = db.collection("user").document(sessions.get(0).getPhone()).get();
            User user = futureUser.get().toObject(User.class);
            if (user != null && user.getStatus() == roleAdmin) {
                ApiFuture<QuerySnapshot> futureEvent = db.collection("event").limit(20).get();
                List<Event> events = futureEvent.get().toObjects(Event.class);
                model.addAttribute("events", events);
                return "listevent";
            } else {
                return "index";
            }
        } else {
            return "login";
        }

    }
    @RequestMapping(value = "admineditorder{id}{status}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String editOrder(Model model, @RequestParam("id") String id, @RequestParam("status") String status) throws ExecutionException, InterruptedException {
        Map<String, Object> map = new HashMap<>();
        System.out.println("eventeditorder");
        Firestore db = FirestoreClient.getFirestore(FirestoreConfig.app);
        ApiFuture<DocumentSnapshot> futureOrder = db.collection("order").document(id).get();
        Order order = futureOrder.get().toObject(Order.class);
        ApiFuture<DocumentSnapshot> futureEvent = db.collection("event").document(order.getEvent()).get();
        Event event = futureEvent.get().toObject(Event.class);
        if (order != null) {
            order.setStatus(status);
            if(status == "paid"){
                event.setSold(event.getSold()+1);
                db.collection("event").document(order.getEvent()).set(event);
            }
            db.collection("order").document(id).set(order);

            map.put("status", 1);
//            map.put("message", "book ticket completed");
        } else {
            map.put("status", 0);
//            map.put("message", "book ticket error");
        }
        return RESTUtil.parJson(map);
    }
    @RequestMapping(value = "admineditevent", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    String editEvent(Model model, @RequestBody() String content, HttpServletResponse resp) throws ExecutionException, InterruptedException, ParseException {
        Map<String, Object> map = new HashMap<>();
        System.out.println("eventeditorder");
        Firestore db = FirestoreClient.getFirestore(FirestoreConfig.app);
        JSONObject object = new JSONObject(content);
        ApiFuture<DocumentSnapshot> futureUser = db.collection("event").document(object.getString("id")).get();
        Event event = futureUser.get().toObject(Event.class);
        if (event != null) {
            event.setTitle(object.getString("title"));
            event.setAddress(object.getString("address"));
            event.setDescription(object.getString("description"));
            event.setPlace(object.getString("place"));
            event.setPrice(Long.parseLong(object.getString("price")));
            event.setQuantity(Long.parseLong(object.getString("quantity")));
            event.setSold(Long.parseLong(object.getString("sold")));
            event.setTime(object.getLong("time"));
            event.setType(object.getString("type_eve"));
            event.setImage(object.getString("image"));
            db.collection("event").document(object.getString("id")).set(event);
            return "{\"status\":1}";
        }else return "{\"status\":2}";
    }


    @RequestMapping(value = "editevent{id}", method = RequestMethod.GET)
    String getEventDetail(Model model, @RequestParam("id") String id) throws ExecutionException, InterruptedException {
        Firestore db = FirestoreClient.getFirestore(FirestoreConfig.app);
            ApiFuture<DocumentSnapshot> futureEvent = db.collection("event").document(id).get();
            Event event = futureEvent.get().toObject(Event.class);
            model.addAttribute("event", event);
            return "editevent";
    }
    @RequestMapping(value = "addevent", method = RequestMethod.GET)
    String getEventDetail(Model model) throws ExecutionException, InterruptedException {
            return "addevent";

    }

    @RequestMapping(value = "editorder{id}", method = RequestMethod.GET)
    String getOrderDetail(Model model, @RequestParam("id") String id) throws ExecutionException, InterruptedException {
        Firestore db = FirestoreClient.getFirestore(FirestoreConfig.app);
        ApiFuture<DocumentSnapshot> futureOrder = db.collection("order").document(id).get();
        Order order = futureOrder.get().toObject(Order.class);
        model.addAttribute("order", order);
        return "editorder";
    }

}
