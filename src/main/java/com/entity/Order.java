package com.entity;

import com.util.Generate;

public class Order {
    private String id;
    private String user;
    private long create_at;
    private long pay_deadline;
    private String event;
    private String contact;
    private String type; // normal - vip
    private String pay;     // visa|transfer|direct|wallet
    private String status; // cancel / wait / paid
    private int quantity;
    private long total_price;

    public Order() {
    }

    public Order(String pay) {
        this.id= Generate.randomAlphaNumeric(20);
        this.pay = pay;
        this.create_at=System.currentTimeMillis();
    }

    public long getTotal_price() {
        return total_price;
    }

    public void setTotal_price(long total_price) {
        this.total_price = total_price;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getPay_deadline() {
        return pay_deadline;
    }

    public void setPay_deadline(long pay_deadline) {
        this.pay_deadline = pay_deadline;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPay() {
        return pay;
    }

    public void setPay(String pay) {
        this.pay = pay;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public long getCreate_at() {
        return create_at;
    }

    public void setCreate_at(long create_at) {
        this.create_at = create_at;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }



    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}